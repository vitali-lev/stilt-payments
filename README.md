### Cron for uploading payments to NSL.

designed to be deployed as **gcloud function**.
(see main.py)

Cron/triggered function for uploading payments to **NLS** 
and synchronizing some data from NLS to DB. 
NLS takes new payments from users and recalculates 
different loan data:
- balances
- amortization schedule.
- dates
  
*see stilt_nls.NLSWrapper.loan_details for more details*
