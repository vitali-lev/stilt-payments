"""Queries to retrieve uncommitted ach payments"""

base_payments_query = """
SELECT
    lpdl.loan_id,
    lpdl.amount,
    lpdl.payment_date_nls,
    l.nls_id,
    lpdl.payment_timestamp,
    lpdl.principal,
    lpdl.interest,
    lpdl.force_accrue,
    lpdl.origination_fee,
    l.old_db_id,
    u.username AS user_email,
    lpdl.ach_payment,
    lpdl.payoff,
    lpdl.autopay,
    lpdl.extra_payment,
    atr.registry_id AS ach_registry_id,
    atr.ach_id,
    atr.ach_batch_id,
    atr.sender_bank_account_id AS stilt_bank_account_id,
    atr.amount AS ach_amount,
    l.interest_balance,
    null AS shadow_interest_balance,
    atr.completion_date_pst
FROM
    loan_payments_daily_ledger lpdl

    JOIN loans l ON (lpdl.loan_id = l.id AND NOT l.deleted)

    JOIN user_loans ul ON (ul.loan_id = l.id AND NOT ul.deleted)

    JOIN users u ON (u.id = ul.user_id AND NOT u.deleted)

    LEFT JOIN ach_transactions_registry atr ON (
        lpdl.ach_registry_id = atr.registry_id AND NOT atr.deleted
    )
WHERE
    NOT lpdl.committed
    AND NOT lpdl.deleted
    AND NOT lpdl.skip_nls
    AND NOT lpdl.returned
    AND lpdl.payment_timestamp <= now()
"""

order_query = """
ORDER BY
    lpdl.loan_id,
    lpdl.payment_date_nls;
"""

query_by_id = (
        base_payments_query +
        "AND lpdl.ach_registry_id IN ({})" +
        order_query
)
query_all = base_payments_query + order_query
