import os

from stilt_payments.config.base import *  # noqa
from stilt_payments.config.constants import * # noqa

if os.environ.get('ENVIRONMENT') == 'prod':
    from stilt_payments.config.prod import *  # noqa
else:
    from stilt_payments.config.stage import *  # noqa

from stilt_payments.config.local import *  # noqa
