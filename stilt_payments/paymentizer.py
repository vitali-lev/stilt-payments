import json
import logging
from datetime import datetime
from typing import List

from google.cloud import error_reporting
from internal.mysql.apis import SQLConnector
from stilt_constants.serverconstants import Constants
from stilt_loan_control.details.loan_details import LoanDetails
from stilt_loan_control.models.loan import Loan
from stilt_loan_control.payment.loan_payment import LoanPayment
from stilt_nls import NLSWrapper, LoanPaymentInfo

from stilt_payments import config
from stilt_payments.payments_queries import query_all, query_by_id

LOGGER = logging.getLogger(__name__)

error_client = error_reporting.Client()
nls_wrapper: NLSWrapper = NLSWrapper(credentials=config.nls_info)
sql_connector_common = SQLConnector(
    db_name=config.RDS_DB_NAME,
    rdb_user=config.RDS_DB_USER,
    rdb_password=config.RDS_DB_PASSWORD,
    rds_host=config.RDS_SERVER_HOST,
)

global_config: Constants = Constants(sql_connector_common)
global_config_map: dict = global_config.constants_dict


def handler(ach_registry_id_list: List[str] = None):
    """
    Cron/triggered function for uploading payments to NLS and synchronizing
    some data from NLS to DB.
    NLS takes new payments from users and recalculate different loan data,
    like balances and amortization schedule.

    :param ach_registry_id_list: list of payments to be processed,
    if not specified, all uncommitted payments will be processed
    :return: None
    """
    start_time = datetime.now()
    LOGGER.info(
        "Starting processing payments. "
        f"registry_id list: {ach_registry_id_list}"
    )

    if ach_registry_id_list:
        registry_id_in = ','.join([f"'{i}'" for i in ach_registry_id_list])
        query = query_by_id.format(registry_id_in)
    else:
        query = query_all

    uncommitted_loan_payments: list = sql_connector_common.run_select(
        query
    )

    num_payments_committed = 0

    payments = []
    for item in uncommitted_loan_payments:
        payment_info = LoanPaymentInfo(
            loan_id=item['loan_id'],
            nls_id=item['nls_id'],
            amount=item['amount'],
            payment_date=item['payment_date_nls'],
            principal=item['principal'],
            interest=item['interest'],
            accrue=item['force_accrue'],
            payoff=item['ach_payment'],
            extra_payment=item['extra_payment'],
            loan_interest_balance=item['interest_balance'],
            ach_batch_id=item['ach_batch_id'],
            ach_completion_date=item['completion_date_pst'],
            ach_id=item['ach_id'],
            payment_method=config.nls_payment_method_code_ach if (
                    item['ach_payment'] in [True, 1, "1"]
            ) else config.nls_payment_method_code_manual,
            registry_id=item['ach_registry_id']
        )
        payments.append(payment_info)

        LOGGER.info(f"Loan ID being processed: {item['loan_id']}")
        LOGGER.info(
            f"Processing payment of ${item['amount']} "
            f"on {item['payment_date_timestamp']}"
        )
        LOGGER.info("Recording payment in NLS ")

    for result, payment in nls_wrapper.record_payments_extended(
            batch_size=1,
            *payments,
    ):
        if not result:
            data = {
                "subject": "NLS Payment not Recorded",
                "data": {
                    "nls_date": str(payment.payment_date),
                    "amount": payment.amount,
                    "nls_id": payment.nls_id,
                },
            }
            error_client.report(message=json.dumps(data))

        # Back sync payment split and loan balances in the loans table
        # if the payment went through and mark payment as committed in
        # daily ledger
        # Marking origination fee payments as committed need to be
        # treated differently (for now) since they don't have corresponding
        # ACH registry IDs. This should eventually change.
        if payment.origination_fee is True:
            sql_connector_common.update(
                "loan_payments_daily_ledger",
                {
                    "committed": True,
                },
                {
                    "committed": False,
                    "deleted": False,
                    "loan_id": payment.loan_id,
                    "origination_fee": True,
                }
            )

        # Save payment split back sync and mark payments as committed
        # for non origination-fee payments
        else:
            current_registry_id: str = payment.registry_id
            loan_details: LoanDetails = LoanDetails(
                connector=sql_connector_common,
                loan_idv2=payment.loan_id,
                nls_info=config.nls_info,
                nls_wrapper=nls_wrapper,
            )

            # Back sync payment split
            loan_payments: dict = loan_details.fetch_payments_for_admin_v2(
            )

            loan_payment: LoanPayment = LoanPayment(
                loan_id=payment.loan_id,
                sql_connector=sql_connector_common,
            )

            loan_payment.save_split_in_ledger(
                registry_id=current_registry_id,
                loan_payments=loan_payments,
            )

            loan_payment.mark_payment_committed_daily_ledger(
                registry_id=current_registry_id,
            )

        num_payments_committed += 1

        # Update loan balances in loans table
        loan_to_sync: Loan = Loan(
            loan_id=payment.loan_id,
            sql_connector=sql_connector_common,
        )
        nls_loan_details: dict = nls_wrapper.loan_details(
            loan=loan_to_sync,
        )
        loan_to_sync.update(update_info=nls_loan_details)

    end_time = datetime.now()

    LOGGER.info(
        f"Loan payments daemon ended. Total execution time: "
        f"{end_time - start_time}. "
        f"Number of payments marked as committed {num_payments_committed}."
    )
