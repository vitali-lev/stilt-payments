from stilt_payments.paymentizer import handler


def paymentizer(request):
    """Gcloud function for Paymentizer cron"""
    ach_registry_id_list = request.get_json().get('ach_registry_id_list')
    handler(ach_registry_id_list)
